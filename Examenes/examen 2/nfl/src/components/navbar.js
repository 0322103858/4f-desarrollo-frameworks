import React from 'react';
import { Container, Nav, Navbar } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import logo from '../assets/NFL.png';

function NavbarComponent() {
  return (
    <Navbar bg="dark" variant="dark">
      <Container>
        <Navbar.Brand href="/">
          <img
            src={logo}
            alt="NFL Logo"
            width="30"
            height="30"
            className="d-inline-block align-top"
          />
          {' NFL ToDo'}
        </Navbar.Brand>
        <Nav className="me-auto">
          <Nav.Link href="/pendientes1">pendientes (solo Id's)</Nav.Link>
          <Nav.Link href="/pendientes2">pendientes (IDs y Titles)</Nav.Link>
          <Nav.Link href="/pendientes3">pendientes Sin resolver (ID y Titles)</Nav.Link>
          <Nav.Link href="/pendientes4">pendientes Resueltos (ID y Titles)</Nav.Link>
          <Nav.Link href="/pendientes5">pendientes (IDs y UserIDs)</Nav.Link>
          <Nav.Link href="/pendientes6">pendientes Sin resolver (IDs y UserIDs)</Nav.Link>
          <Nav.Link href="/pendientes7">pendientes Resueltos (IDs y UserIDs)</Nav.Link>
        </Nav>
      </Container>
    </Navbar>
  );
}

export default NavbarComponent;
