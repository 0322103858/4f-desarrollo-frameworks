import React, { useState, useEffect } from 'react';
import axios from 'axios';

function PendientesTre() {
  const [incompleteTodos, setIncompleteTodos] = useState([]);

  useEffect(() => {
    fetchData();
  }, []);

  const fetchData = async () => {
    try {
      const response = await axios.get('https://jsonplaceholder.typicode.com/todos');
      const incompleteData = response.data.filter(todo => !todo.completed);
      setIncompleteTodos(incompleteData);
    } catch (error) {
      console.error('Error al obtener datos:', error);
    }
  };

  return (
    <div>
      <h1>Pendientes No Completados</h1>
      <table>
        <thead>
          <tr>
            <th>ID</th>
            <th>Título</th>
          </tr>
        </thead>
        <tbody>
          {incompleteTodos.map(todo => (
            <tr key={todo.id}>
              <td>{todo.id}</td>
              <td>{todo.title}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}

export default PendientesTre;
