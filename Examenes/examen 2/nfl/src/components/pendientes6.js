import React, { useState, useEffect } from 'react';
import axios from 'axios';

function PendientesSei() {
  const [completedTodos, setCompletedTodos] = useState([]);

  useEffect(() => {
    fetchData();
  }, []);

  const fetchData = async () => {
    try {
      const response = await axios.get('https://jsonplaceholder.typicode.com/todos?completed=true');
      setCompletedTodos(response.data);
    } catch (error) {
      console.error('Error al obtener datos:', error);
    }
  };

  return (
    <div>
      <h1>Pendientes Completados</h1>
      <table>
        <thead>
          <tr>
            <th>ID</th>
            <th>UserID</th>
          </tr>
        </thead>
        <tbody>
          {completedTodos.map(todo => (
            <tr key={todo.id}>
              <td>{todo.id}</td>
              <td>{todo.userId}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}

export default PendientesSei;
