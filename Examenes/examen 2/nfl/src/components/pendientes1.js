import React, { useState, useEffect } from 'react';

function PendientesUno() {
  const [todoIds, setTodoIds] = useState([]);

  useEffect(() => {
    fetchData();
  }, []);

  const fetchData = async () => {
    try {
      const response = await fetch('https://jsonplaceholder.typicode.com/todos');
      const data = await response.json();
      const ids = data.map(todo => todo.id);
      setTodoIds(ids);
    } catch (error) {
      console.error('Error al obtener datos:', error);
    }
  };

  return (
    <div>
      <h1>Lista de IDs</h1>
      <ul>
        {todoIds.map(id => (
          <li key={id}>{id}</li>
        ))}
      </ul>
    </div>
  );
}

export default PendientesUno;
