import React from 'react';
import miImagen from '../assets/NFL.png';

function Home() {
  return (
    <div>
      <h1>Bienvenido a la pizarra de pendientes de la NFL (To Do)</h1>
      <p>Selecciona un menu para comenzar !</p>
      <center><img src={miImagen} alt="Mi imagen" /></center>
    </div>
  );
}

export default Home;
