import React, { useState, useEffect } from 'react';
import axios from 'axios';

function PendientesSie() {
  const [incompleteTodos, setIncompleteTodos] = useState([]);

  useEffect(() => {
    fetchData();
  }, []);

  const fetchData = async () => {
    try {
      const response = await axios.get('https://jsonplaceholder.typicode.com/todos?completed=false');
      setIncompleteTodos(response.data);
    } catch (error) {
      console.error('Error al obtener datos:', error);
    }
  };

  return (
    <div>
      <h1>Pendientes No Completados</h1>
      <table>
        <thead>
          <tr>
            <th>ID</th>
            <th>UserID</th>
          </tr>
        </thead>
        <tbody>
          {incompleteTodos.map(todo => (
            <tr key={todo.id}>
              <td>{todo.id}</td>
              <td>{todo.userId}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}

export default PendientesSie;
