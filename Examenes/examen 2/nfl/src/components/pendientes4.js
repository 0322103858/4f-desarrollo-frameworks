import React, { useState, useEffect } from 'react';
import axios from 'axios';

function PendientesCua() {
  const [completedTodos, setCompletedTodos] = useState([]);

  useEffect(() => {
    fetchData();
  }, []);

  const fetchData = async () => {
    try {
      const response = await axios.get('https://jsonplaceholder.typicode.com/todos');
      const completedData = response.data.filter(todo => todo.completed);
      setCompletedTodos(completedData);
    } catch (error) {
      console.error('Error al obtener datos:', error);
    }
  };

  return (
    <div>
      <h1>Pendientes Completados</h1>
      <table>
        <thead>
          <tr>
            <th>ID</th>
            <th>Título</th>
          </tr>
        </thead>
        <tbody>
          {completedTodos.map(todo => (
            <tr key={todo.id}>
              <td>{todo.id}</td>
              <td>{todo.title}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}

export default PendientesCua;
