import React from 'react';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import NavbarComponent from './components/navbar';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';

import Home from './components/home';
import PendientesUno from './components/pendientes1';
import PendientesDos from './components/pendientes2';
import PendientesTre from './components/pendientes3';
import PendientesCua from './components/pendientes4';
import PendientesCin from './components/pendientes5';
import PendientesSei from './components/pendientes6';
import PendientesSie from './components/pendientes7';

function App() {
  return (
    <Router>
      <NavbarComponent />
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/pendientes1" element={<PendientesUno />} />
        <Route path="/pendientes2" element={<PendientesDos />} />
        <Route path="/pendientes3" element={<PendientesTre />} />
        <Route path="/pendientes4" element={<PendientesCua />} />
        <Route path="/pendientes5" element={<PendientesCin />} />
        <Route path="/pendientes6" element={<PendientesSei />} />
        <Route path="/pendientes7" element={<PendientesSie />} />
      </Routes>
    </Router>
  );
}

export default App;
