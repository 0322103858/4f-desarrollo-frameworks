import React from 'react';
import './CSS/Footer.css'; 

function Footer() {
  return (
    <footer className="footer">
      <p>© 2024 Serendipity</p>
    </footer>
  );
}

export default Footer;