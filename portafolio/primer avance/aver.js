/*
const axios = require('axios');

var url = "https://jsonplaceholder.typicode.com/posts"

axios.post(url, {
    userId: 2,
    title: "Lorem Ipsum ETC ETC"
}).then(({data}) => console.log(data))

*/

/*
var url = "https://jsonplaceholder.typicode.com/albums"

axios.post(url, {
    


}).then(({data}) => console.log(data))
*/

const axios = require('axios');

// Obtener todos los álbumes
axios.get("https://jsonplaceholder.typicode.com/albums")
  .then(response => {
    const albums = response.data;
    console.log("Todos los álbumes:");
    console.log(albums);

    // Agregar un nuevo álbum
    const newAlbum = {
      userId: 2,
      title: "Nuevo Álbum"
    };

    return axios.post("https://jsonplaceholder.typicode.com/albums", newAlbum);
  })
  .then(response => {
    const addedAlbum = response.data;
    console.log("Álbum añadido:");
    console.log(addedAlbum);
  })
  .catch(error => {
    console.error("Error:", error.message);
  });
