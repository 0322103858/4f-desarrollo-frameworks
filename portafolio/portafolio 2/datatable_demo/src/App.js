import './App.css';
import PokemonTable from './components/PokemonTable';
// aqui solo importo el index  en pokemontables y lo mando a llamar a mi app.js
//bastante simple,la app solo muestra ese componente 

function App() {
  return (
    <div className="App">
      <PokemonTable />
    </div>
  );
}

export default App;