import React, { useState, useEffect } from "react";
import MUIDataTable from "mui-datatables";
import { Grid } from "@mui/material";

//instale las dependencias de mui materialss y styles
//tambien la dependenncia de datatables y axios aunque no lo use, primero intente usar una api de star wars 
//y llamarla con axios pero no  servia la api de star wars, asi que la reemplaze siguiendo un tutorial de una api de pokemon 

const PokemonTable = () => {
    const [data, setData] = useState([]);
    const API = 'https://pokeapi.co/api/v2/pokemon?limit=20'; //limito los datos que se muestran a 20 por pagina
    useEffect(() => {
        const fetchData = async () => { //aplico fetch para jalar la api 
            try {
                const response = await fetch(API);
                const results = await response.json();
                // console.log(results);
                // console.log(results.results);
                // setData(results.results);
                const pokemonData = await Promise.all(results.results.map(async (result) => {
                    const pokemonResponse = await fetch(result.url);
                    const pokemonData = await pokemonResponse.json();
                    console.log(pokemonData);
                    //regresa la id, nombre e imagen del pokemon
                    return {
                        id: pokemonData.id,
                        name: pokemonData.name,
                        image: pokemonData.sprites.front_default
                    }
                }));
                setData(pokemonData);
            } catch (error) {
                console.log(error);
            }
        };

        fetchData();
    }, []);

    // geenera 3 columnass
    const columns = [
        {
            label: 'Id',
            name: 'id'
        },
        {
            name: 'name',
            label: 'Pokemon'
        },
        {
            label: 'Image',
            name: 'image',
            options: {
                customBodyRender: (value) => (
                    <img
                        src={value}
                        alt="pokemon"
                        style={{width: '50px', height: '50px'}}
                    />
                )
            }
        }
    ];
    const options = {};

    //formato de las tablas
    return (
        <Grid container spacing={2} sx={{ padding: '24px' }}>
            <Grid item xs={12}>
                Pokemon Table
            </Grid>
            <Grid item xs={12}>
                <MUIDataTable data={data} columns={columns} options={options} />
            </Grid>
        </Grid>
    );
};

export default PokemonTable;