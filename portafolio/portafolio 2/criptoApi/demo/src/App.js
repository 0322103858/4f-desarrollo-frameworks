import React, { useState, useEffect } from 'react';
import './App.css';

//esta es la primera practica con api  donde usamos fetch para utilizar la api de crypto

const url = "https://api.coindesk.com/v1/bpi/currentprice.json";

export default function App() {
    const [data, setData] = useState(null);
    const [isLoading, setIsLoading] = useState(true);
    const [error, setError] = useState(null);

    useEffect(() => {
        fetch(url)
            .then(response => response.json())
            .then(result => {
                setIsLoading(false);
                setData(result);
            })
            .catch(error => {
                setIsLoading(false);
                setError(error);
            });
    }, []);

    if (isLoading) {
        return <div className="App"><h4>Loading Data...</h4><progress value={50} /></div>;
    }

    if (error) {
        return <h4>Error: {error.message}</h4>;
    }

    return (
        <div className="App">
            <h1>BTC to USD|EUR|GBP</h1>
            <h3>BTC to USD</h3>
            <table>
                <thead>
                    <tr>
                        <th>RATE</th>
                        <th>RATE FLOAT</th>
                        <th>DESCRIPTION</th>
                        <th>UPDATED</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>{data["bpi"]["USD"].rate} </td>
                        <td>{data["bpi"]["USD"].rate_float} </td>
                        <td>{data["bpi"]["USD"].description} </td>
                        <td>{data["time"].updated} </td>
                    </tr>
                </tbody>
            </table>
        </div>
    );
}
