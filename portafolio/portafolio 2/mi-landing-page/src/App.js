//imporrto botstrap y los componentes qque utilizo
import React from 'react';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import Form from './components/Form';

import MyFooter from './components/Footer';
import BasicExample from './components/Navbar';
import UncontrolledExample from './components/Carusel';
import GroupExample from './components/Card';

//aqui hago que se muestren
function App() {
  return (
    <div className="App">
      <BasicExample />
      <UncontrolledExample />
      <Form />
      <GroupExample />
      <MyFooter />
    </div>
  );
}

export default App;
