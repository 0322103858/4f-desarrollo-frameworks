import React, { useState } from 'react';
import '../App.css';  // importa estilos, este es el formulario de nombre y apellido

export default function Form() {
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');

    const fullName = firstName + ' ' + lastName;

    function handleFirstNameChange(e) {
        setFirstName(e.target.value);
    }

    function handleLastNameChange(e) {
        setLastName(e.target.value);
    }

    return (
        <>
            <h2>SignUp</h2>
            <div className="row">
                <div className="col-8 form-wrapper"> {/* Agrega la clase form-wrapper */}
                    <form action="" class="form">
                        <label htmlFor="firstName">First Name:</label>
                        <input class="form-control" type="text" value={firstName} onChange={handleFirstNameChange} />

                        <label htmlFor="lastName">Last Name:</label>
                        <input class="form-control" type="text" value={lastName} onChange={handleLastNameChange} />
                        <button class="btn btn-outline-success form-control">SignUp</button>
                    </form>
                </div>
            </div>

            <p>Bienvenido: <strong>{fullName}</strong></p>
        </>
    );
}
