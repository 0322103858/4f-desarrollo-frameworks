import React from 'react';
import Carousel from 'react-bootstrap/Carousel';
import { Image } from 'react-bootstrap';
import Rojo from '../assets/images/rojo.jpg';
import Azul from '../assets/images/azul.png';
import Verde from '../assets/images/verde.jpeg';
import '../assets/css/CarouselStyles.css'; //Importe imagenes y estilos del carusel sacado de la pagina oficial de bootsstrap

function UncontrolledExample() {
  return (
    <Carousel>
      <Carousel.Item>
        <Image src={Rojo} className="carousel-image" alt="Your Image" />
        <Carousel.Caption>
          <h3 className="black-title">First slide</h3>
          <p> </p>
        </Carousel.Caption>
      </Carousel.Item>
      <Carousel.Item>
        <Image src={Verde} className="carousel-image" alt="Your Image" />
        <Carousel.Caption>
          <h3 className="black-title">Second slide</h3>
          <p> </p>
        </Carousel.Caption>
      </Carousel.Item>
      <Carousel.Item>
        <Image src={Azul} className="carousel-image" alt="Your Image" />
        <Carousel.Caption>
          <h3 className="black-title">Third slide</h3>
          <p> </p>
        </Carousel.Caption>
      </Carousel.Item>
    </Carousel>
  );
}

export default UncontrolledExample;
