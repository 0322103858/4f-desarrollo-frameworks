// pie de paggina sacado de la pagina oficial de  bootstrap
import React from 'react';

const MyFooter = () => {
  return (
    <footer className="bg-dark text-light text-center py-3">
      © 2024 Mi Página. Todos los derechos reservados.
    </footer>
  );
};

export default MyFooter;
